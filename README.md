
研究员：桑杰逊，最新修改于： 十月 26, 2020 
⭐整理技术文献⭐


OPC（OLE for Process Control）定义：指为了给工业控制系统应用程序之间的通信建立一个接口标准，在工业控制设备与控制软件之间建立统一的数据存取规范。它给工业控制领域提供了一种标准数据访问机制，将硬件与应用软件有效地分离开来，是一套与厂商无关的软件数据交换标准接口和规程，主要解决过程控制系统与其数据源的数据交换问题，可以在各个应用之间提供透明的数据访问。实际项目中“设备”就变成一个可以访问的OPC Server和它的Tag位号值。

OPC实时数据访问规范（OPC DA）定义了包括数据值，更新时间与数据品质信息的相关标准。
OPC历史数据访问规范（OPC HDA）定义了查询、分析历史数据和含有时标的数据的方法。
OPC报警事件访问规范（OPC AE）定义了报警与时间类型的消息类信息，以及状态变化管理等相关标准。

OPC 是基于微软的DCOM 技术，用于设备和软件之间交换数据。 这也意味着OPC 只能在window 系统上运行.在运行OPC 之前需要配置Window 的COM/DCOM.

OPC DA（旧接口方案）




是DA 2.0的，因此找到了以下两个开源类库。
JEasyOPC Client

底层依赖JNI，只能跑在windows环境，不能跨平台
整个类库比较古老，使用的dll是32位的，整个项目只能使用32位的JRE运行
同时支持DA 2.0与3.0协议，算是亮点
JeasyOPC：（不支持跨平台）

博客参考：

https://blog.csdn.net/qq_33720460/article/details/78478430

https://blog.csdn.net/wangzhi291/article/details/45029799

https://blog.csdn.net/diyu122222/article/details/77645668




 ** :zap: OPC UA 统一架构 最新标准** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/1105/105455_d2eb36c0_5463068.png "屏幕截图.png")

https://www.oracle.com/downloads/cloud/iot-gateway-downloads.html

https://docs.oracle.com/en/cloud/saas/iot-production-cloud/tutorial-iot-opcua-gateway/index.html



http://www.motrotech.com/OPCUA.html

https://opcfoundation.org/about/opc-technologies/opc-ua/

国家标准
标准号：GB/T 33863.1-2017 
中文标准名称：OPC统一架构 第1部分：概述和概念
英文标准名称：OPC unified architecture—Part 1: Overview and concepts

标准状态：现行



发布日期中国标准分类号（CCS）
N10
国际标准分类号（ICS）
25.040

2017-07-12
实施日期
2018-02-01
主管部门
中国机械工业联合会
归口单位
全国工业过程测量控制和自动化标准化技术委员会
发布单位
中华人民共和国国家质量监督检验检疫总局、中国国家标准化管理委员会




1. 功能等价：所有的基于COM的OPC规范中的功能，都映射到了OPC UA中。

2. 多平台支持：支持从嵌入式的微控制器到基于云的分散式控制架构。

3. 安全：信息加密，互访认证以及安全监听功能。OPC UA使用了OpenSSL许可证来规定哪些应用程序或系统可以使用OPC与另一端相连接。

4. 扩展性：不影响现有应用程序的情况下，就可以添加新的功能。

5. 丰富的信息建模：可定义复杂的信息，而不再是单一的数据。







OPC采集方式

OPC对象有三个：服务器(OPC Server)、组(OPC Group)和项(OPC Item)

OPC UA驱动和OPC UA设备的连接是通过OPC UA服务器关联的，OPC UA驱动通过操作OPC UA服务器对外暴露的协议接口操作OPC UA设备，示意图如下所示。


服务端实现方案：
1.KEPServer集成了OPC运行库，所以不需要单独安装。（试用版-收费）

参考：https://www.cnblogs.com/ioufev/p/9366877.html

2.Matrikon OPC Server Simulation
模拟仿真本文采用此方案。
参考：https://www.cnblogs.com/ioufev/p/9366426.html


自己开发Eclipse-Milo JAVA框架
https://blog.csdn.net/q932104843/article/details/86664236?utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~first_rank_v2~rank_v25-2-86664236.nonecase&utm_term=ua线程%20提高opc



https://www.cnblogs.com/myboat/p/11891148.html

https://github.com/s5uishida/milo-example-server

https://www.cnblogs.com/myboat/p/11890308.html

http://opcfoundation.github.io/UA-Java-Legacy/

https://github.com/OPCFoundation/UA-Java-Legacy

https://github.com/blasty3/SOSJ-OPCUA

https://github.com/dfki-iui/opcua4oms#ch2



 **_测试 OPC读取RFID读写头信息（刀具信息）_** 

