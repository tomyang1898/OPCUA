import org.jinterop.dcom.common.JIException;import org.openscada.opc.lib.common.ConnectionInformation;import org.openscada.opc.lib.da.AccessBase;import org.openscada.opc.lib.da.Server;import org.openscada.opc.lib.da.SyncAccess;
import java.util.concurrent.Executors;
public class UtgardTutorial1 {

    public static void main(String[] args) throws Exception {
        // create connection information
        final ConnectionInformation ci = new ConnectionInformation();
        ci.setHost("localhost");
        ci.setUser("ASUS");
        ci.setPassword("");
        ci.setProgId("TLSvrRDK.OPCTOOLKIT.DEMO");//        ci.setClsid("08a3cc25-5953-47c1-9f81-efe3046f2d8c"); // if ProgId is not working, try it using the Clsid instead
        final String itemId = "tag1";
        // create a new server
        final Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        try {
            // connect to server
            server.connect();
            // add sync access, poll every 500 ms
            final AccessBase access = new SyncAccess(server, 500);
            access.addItem(itemId, (item, state) ->
                    System.out.println("Resut: " + state.toString()));
            // start reading
            access.bind();
            // wait a little bit
            Thread.sleep(10 * 1000);
            // stop reading
            access.unbind();
        } catch (final JIException e) {
            System.out.println(String.format("%08X: %s", e.getErrorCode(), server.getErrorMessage(e.getErrorCode())));
            e.printStackTrace();
        }
    }
}
